package profile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Niveaux extends CommunM implements Commun{
	
	private BorderPane root = new BorderPane();
	private StackPane spLeft = new StackPane();
	private StackPane spRight = new StackPane();
	private static GridPane niveaux = new GridPane();
	private static VBox vbox = new VBox();
	private static int numProfil;
	private final Label labelRetour = new Label("← Retour");
	private static ArrayList<Circle> cercles = new ArrayList<Circle>();
	
	public Niveaux() {
		definirFenetre();
		labelRetour.setOnMouseClicked(e -> Principale.toListProfilJ());
		this.getScene().setOnKeyPressed(e -> sceneAction(e));	
	}
	public static void updateProfil() {
		vbox.getChildren().clear();
		vbox.getChildren().add(ListeProfil.getListeProfil().get(numProfil).getProfilNiveaux());
		vbox.getChildren().add(niveaux);
		VBox.setMargin(ListeProfil.getListeProfil().get(numProfil).getProfilNiveaux(), new Insets(30,0,110,0));
		vbox.setPrefWidth(windowWidth-275*2);
		for(int i=0;i<cercles.size();i++) {
			if(ListeProfil.getListeProfil().get(numProfil).getAvancer()>i) {
				cercles.get(i).setFill(rouge);
			}else {
				cercles.get(i).setFill(Color.LIGHTGRAY);
			}
		}	
	}
	protected Parent creerContenu() {
		root.setStyle(couleurFond);
	
		labelRetour.setTextFill(jaune);
		labelRetour.setFont(cooperHewitt50);
		
		updateProfil();
		
		int k=1;
		for(int a=0;a<2;a++) {
			for(int b=0;b<3;b++) {
				niveaux.add(creationBoutonNiveau(k), b, a);
				k++;
			}
			
		}
		niveaux.setAlignment(Pos.CENTER);
		niveaux.setHgap(75);
		niveaux.setVgap(15);
		
		Image decoHautDroit = null;
		Image decoHautGauche = null;
		try {
			decoHautDroit = new Image(new FileInputStream("src/img/decoHautDroit.png"));
			decoHautGauche = new Image(new FileInputStream("src/img/decoHautGauche.png"));
		} catch (FileNotFoundException e) {System.out.println("Fichier non trouvé");}
		ImageView decoHD = new ImageView(decoHautDroit);
		ImageView decoHG = new ImageView(decoHautGauche);
		decoHD.setPreserveRatio(true);
		decoHG.setPreserveRatio(true);
		decoHD.setFitHeight(250);
		decoHG.setFitHeight(250);
		
		spLeft.getChildren().addAll(decoHG,labelRetour);
		spRight.getChildren().add(decoHD);
		StackPane.setAlignment(decoHD, Pos.TOP_RIGHT);
		StackPane.setAlignment(decoHG, Pos.TOP_LEFT);
		StackPane.setAlignment(labelRetour, Pos.CENTER);
		
		BorderPane.setAlignment(niveaux,Pos.CENTER);
		
		root.setLeft(spLeft);
		root.setRight(spRight);
		root.setCenter(vbox);

		return root;
	}
	
	public StackPane creationBoutonNiveau(int i) {
		StackPane bouton = new StackPane();
		Circle c = new Circle(80);
		if(ListeProfil.getListeProfil().get(numProfil).getAvancer()>i) {
			c.setFill(rouge);
		}else {
			c.setFill(Color.LIGHTGRAY);
		}
		cercles.add(c);
		Label l = new Label("Niveau "+i);
		l.setTextFill(Color.WHITE);
		l.setFont(cooperHewitt35);
		bouton.getChildren().addAll(c,l);
		bouton.setOnMouseClicked(e -> {
			int minute=0, seconde=0;
			if(ListeProfil.getListeProfil().get(numProfil).getChoixDifficulter().equals("Facile")) minute=30;
			else if(ListeProfil.getListeProfil().get(numProfil).getChoixDifficulter().equals("Normale")) minute=35;	
			else if(ListeProfil.getListeProfil().get(numProfil).getChoixDifficulter().equals("Difficile")) minute=40;
			
			if(c.getFill().equals(rouge)) {
				Grille.setNumGrille(i);
				Grille.setNumProfil(numProfil);
				Grille.chargerNiveau("niv"+i+".csv", "niv"+i+"c.csv",minute,seconde);
				Principale.toGrille();
			}
		});
		return bouton;
	}
	public void sceneAction(KeyEvent e) {
		obtenirTouches(e);
		if(quitter) {
			Principale.toListProfilJ();
		}
	}
	public static void setNumProfil(int numProfil) {
		Niveaux.numProfil = numProfil;
	}
}