package profile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Font;


public class Grille extends CommunM implements Commun{
	private static StackPane[][] niveau ={{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null},
										{null,null,null,null,null,null,null,null,null}};
	private static String[][] niveauC = {{"","","","","","","","",""},
										{"","","","","","","","",""},
										{"","","","","","","","",""},
										{"","","","","","","","",""},
										{"","","","","","","","",""},
										{"","","","","","","","",""},
										{"","","","","","","","",""},
										{"","","","","","","","",""},
										{"","","","","","","","",""}};

	private final static int nombre = 9;
	private final int taille = 3;
	
	private final int tailleGrille = 600;
	private final int tailleBordGrille = 15;
	private final int tailleSousGrille = (tailleGrille-(tailleBordGrille*4))/3;
	private final int tailleBordSousGrille = 5;
	private final int tailleCaseGrille = (tailleSousGrille-(tailleBordSousGrille*4))/3;
	
	private StackPane root = new StackPane();
	private BorderPane bp = new BorderPane();
	private VBox vbox = new VBox();
	private StackPane boutonPause = new StackPane();
	private Circle cerclePause = new Circle(60,jaune);
	private Label labelPause = new Label("| |");
	private Label labelValider = new Label("✓");
	private static Thread newThread;
	private static Temps timer;
	private static String niv;
	private static String nivC;
	private static int minute;
	private static int seconde;
	private static int numProfil;
	private static int numGrille;

	public Grille() {
		definirFenetre();	
		labelValider.setOnMouseClicked(e-> labelValiderAction());
		boutonPause.setOnMouseClicked(e -> boutonPauseAction());
		this.getScene().setOnKeyPressed(e -> sceneAction(e));
	}
	
	public void avertissement() {
		StackPane root = new StackPane();
		Rectangle fond = new Rectangle(0.45*windowWidth,0.45*windowHeight,Color.WHEAT);
		Label labelAnnonce = new Label(verifierNiveau());
		Label labelRejouer = new Label("Rejouer");
		Label labelMenu = new Label("Menu");
		Label labelTemps = new Label(Temps.getTempsEcoule());
		
		labelAnnonce.setFont(cooperHewitt35);
		labelRejouer.setFont(cooperHewitt35);
		labelMenu.setFont(cooperHewitt35);
		labelTemps.setFont(cooperHewitt35);
		
		labelAnnonce.setTextFill(Color.WHITE);
		labelRejouer.setTextFill(Color.WHITE);
		labelMenu.setTextFill(Color.WHITE);
		labelTemps.setTextFill(Color.WHITE);

		root.getChildren().addAll(fond,labelAnnonce,labelRejouer,labelMenu,labelTemps);
		
		StackPane.setAlignment(labelAnnonce, Pos.TOP_CENTER);
		StackPane.setAlignment(labelRejouer, Pos.BOTTOM_LEFT);
		StackPane.setAlignment(labelMenu, Pos.BOTTOM_RIGHT);
		StackPane.setAlignment(labelTemps, Pos.CENTER);
		StackPane.setAlignment(root, Pos.CENTER);
		StackPane.setMargin(labelAnnonce, new Insets(30,0,0,30));
		StackPane.setMargin(labelRejouer, new Insets(0,0,30,30));
		StackPane.setMargin(labelMenu, new Insets(0,30,30,0));
		
		root.setMaxWidth(0.45*windowWidth);
		root.setMaxHeight(0.45*windowHeight);
		this.root.getChildren().add(root);
		labelRejouer.setOnMouseClicked(e -> avertissementAction(false,root));
		labelMenu.setOnMouseClicked(e -> avertissementAction(true,root));
		this.root.getScene().setOnKeyPressed(e -> {
			obtenirTouches(e);
			if(quitter) {
				avertissementAction(false,root);
			}else if(accepter) { avertissementAction(true,root);
			}else sceneAction(e);
		});
	}
	public void avertissementAction(Boolean res,StackPane root) {
		this.root.getChildren().remove(root);
		Sauvegarder.serialization(); 
		if(res) {
			Principale.toNiveaux();
		}else {
			chargerNiveau(niv,nivC,minute,seconde);
		}
	}

	public static void stoperTimer() {
		Temps.setEnPause(true);
	}
	public static void commencerTimer() {
		Temps.setTerminer(false);
		Temps.setEnPause(false);
		newThread.start();
	}
	public static void reprendreTimer() {
		Temps.setEnPause(false);
	}
	
	protected Parent creerContenu() {
		root.setStyle(couleurFond);
		StackPane grille = creerAffichageGrille();
		Label temps = Temps.getLabelTemps();
		
		labelValider.setTextFill(jaune);
		labelPause.setTextFill(Color.WHITE);
		temps.setTextFill(rouge);
		labelValider.setFont(new Font(120));
		labelPause.setFont(cooperHewitt60);
		temps.setFont(fascinate);
		
		boutonPause.getChildren().addAll(cerclePause,labelPause);
		vbox.getChildren().addAll(boutonPause,temps,labelValider);
		vbox.setPrefWidth(0.30*windowWidth);
		vbox.setAlignment(Pos.CENTER);
		
		root.getChildren().add(bp);
		bp.setLeft(vbox);
		bp.setCenter(grille);
		BorderPane.setAlignment(grille, Pos.CENTER);
		BorderPane.setMargin(vbox, new Insets(0,0,0,50));
		
		return root;
	}

	public static void chargerNiveau(String niv,String nivC,int minute,int seconde) {
		Grille.niv=niv;
		Grille.nivC=nivC;
		Grille.minute=minute;
		Grille.seconde=seconde;
		timer = new Temps(minute,seconde);
		newThread = new Thread(() -> timer.start());
		commencerTimer();
		for(int k=0;k<nombre;k++) {
	    	 for(int i=0;i<nombre;i++) {
		        ((Labeled) niveau[k][i].getChildren().get(niveau[k][i].getChildren().size()-1)).setText("");	
		        ((Shape) niveau[k][i].getChildren().get(0)).setFill(Color.WHITE);
	        	((Shape) niveau[k][i].getChildren().get(1)).setFill(Color.WHITE);
		        niveauC[k][i] = "";
		     }    
	    }
		
		try (BufferedReader br = new BufferedReader(new FileReader(niv)); BufferedReader brC = new BufferedReader(new FileReader(nivC))){
		    for(int k=0;k<nombre;k++) {
		    	 String[] values = br.readLine().split(",");
		    	 String[] valuesC = brC.readLine().split(",");
		    	 for(int i=0;i<nombre;i++) {
			        if (Integer.parseInt(values[i])!=0) {
			        	((Labeled) niveau[k][i].getChildren().get(niveau[k][i].getChildren().size()-1)).setText(values[i]);	
			        	((Shape) niveau[k][i].getChildren().get(0)).setFill(Color.web("#ebebeb"));
			        	((Shape) niveau[k][i].getChildren().get(1)).setFill(Color.web("#ebebeb"));
			        }
			        niveauC[k][i] = valuesC[i];
			     }    
		    }
		} catch (FileNotFoundException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();}
	}
	public String verifierNiveau() {
		int i=0,k=0;
		Boolean res = true;
		while(i<nombre&&res) {
			k=0;
			while(k<nombre&&res) {
				if (!(((Labeled) niveau[k][i].getChildren().get(niveau[k][i].getChildren().size()-1)).getText().equals(niveauC[k][i]))) {
					res = false;
					
				}
				k++;
			}
			i++;
		}
		String string="";
		if(res) {
			string="Victoire !";
			if(ListeProfil.getListeProfil().get(numProfil).getAvancer()==numGrille) {
				ListeProfil.getListeProfil().get(numProfil).setAvancer(numGrille+1);
			}	
		}
		else if(!res) string="Défaite !";
		return string;
	}
	
	public void changerValeurCase() {
		for(int a=0;a<nombre;a++) {
			for(int b=0;b<nombre;b++) {
				if((((Shape) niveau[a][b].getChildren().get(0)).getFill()==bleu)&&!(((Shape) niveau[a][b].getChildren().get(1)).getFill().equals(Color.web("#ebebeb")))) {
					for(int k=1;k<10;k++) {
						if(touche.equals(String.valueOf(k))) {
							((Labeled) niveau[a][b].getChildren().get(niveau[a][b].getChildren().size()-1)).setText(touche);				
						}else if(supprimer){
							((Labeled) niveau[a][b].getChildren().get(niveau[a][b].getChildren().size()-1)).setText("");	
						}
					}
					
				}
			}
		}
	}
	
	public void selectionnerCase() {
		ArrayList<Integer> id = new ArrayList<Integer>();
		if(haut||bas||gauche||droit){
			id = chercherCase();
			int x=id.get(0), z=id.get(1);
			if(x==-1) {
				if(cerclePause.getFill().equals(bleu)&&bas) { labelValider.setTextFill(bleu); cerclePause.setFill(jaune);
				}else if(labelValider.getTextFill().equals(bleu)&&haut) { labelValider.setTextFill(jaune); cerclePause.setFill(bleu);
				}else if((!cerclePause.getFill().equals(bleu)&&!labelValider.getTextFill().equals(bleu))||droit) { labelValider.setTextFill(jaune); cerclePause.setFill(jaune); survolerCase(0,0);}			
			}else {
				if(haut&&x>0) { survolerCase(x-1,z); ((Shape) niveau[x][z].getChildren().get(0)).setFill(((Shape) niveau[x][z].getChildren().get(1)).getFill());
				}else if(bas&&x<8) { survolerCase(x+1,z); ((Shape) niveau[x][z].getChildren().get(0)).setFill(((Shape) niveau[x][z].getChildren().get(1)).getFill());  
				}else if(gauche&&z>0) { survolerCase(x,z-1); ((Shape) niveau[x][z].getChildren().get(0)).setFill(((Shape) niveau[x][z].getChildren().get(1)).getFill()); 
				}else if(droit&&z<8) { survolerCase(x,z+1); ((Shape) niveau[x][z].getChildren().get(0)).setFill(((Shape) niveau[x][z].getChildren().get(1)).getFill());
				}else if(gauche&&z==0) {cerclePause.setFill(bleu); ((Shape) niveau[x][z].getChildren().get(0)).setFill(((Shape) niveau[x][z].getChildren().get(1)).getFill());}
			}
		}
	}	
	public void survolerCase(int x,int z) {
		Rectangle rectangle;
		if(((Shape) niveau[x][z].getChildren().get(1)).getFill().equals(Color.web("#ebebeb"))) {
			rectangle = new Rectangle(tailleCaseGrille-tailleBordSousGrille*2,tailleCaseGrille-tailleBordSousGrille*2,Color.web("#ebebeb"));
		}else {
			rectangle = new Rectangle(tailleCaseGrille-tailleBordSousGrille*2,tailleCaseGrille-tailleBordSousGrille*2,Color.WHITE);
		}
		((Shape) niveau[x][z].getChildren().get(0)).setFill(bleu);
		Label l = new Label(((Labeled) niveau[x][z].getChildren().get(niveau[x][z].getChildren().size()-1)).getText());
		l.setFont(cooperHewitt35);
		niveau[x][z].getChildren().remove(2);
		niveau[x][z].getChildren().remove(1);
		niveau[x][z].getChildren().add(rectangle);
		niveau[x][z].getChildren().add(l);
	}
	public ArrayList<Integer> chercherCase() {
		ArrayList<Integer> id = new ArrayList<Integer>();
		int x=-1,z=-1;
		int a=0,b=0;
		while((a<nombre)&&(x==-1)) {
			b=0;
			while((b<nombre)&&(x==-1)) {
				if(((Shape) niveau[a][b].getChildren().get(0)).getFill().equals(bleu)) {
					x=a;
					z=b;
				}
				b++;
			}
			a++;
		}
		id.add(x);
		id.add(z);
		return id;
	}
	
	public StackPane creerAffichageSousGrille(int z, int x) {
		StackPane sousGrille = new StackPane();
		GridPane cases = new GridPane();
		Rectangle rectangleFondSousGrille = new Rectangle(tailleSousGrille,tailleSousGrille,rouge);
		
		//case
		for(int a=0;a<taille;a++) {
			for(int b=0;b<taille;b++) {
				StackPane affichageCase = new StackPane();
				Rectangle rectangleCaseGrille = new Rectangle(tailleCaseGrille,tailleCaseGrille,Color.WHITE);
				Rectangle rectangle = new Rectangle(tailleCaseGrille-tailleBordSousGrille*2,tailleCaseGrille-tailleBordSousGrille*2,Color.WHITE);
				Label l = new Label();
				l.setFont(cooperHewitt35);
				
				affichageCase.getChildren().addAll(rectangleCaseGrille,rectangle,l);
				niveau[a+(x*3)][b+(z*3)] = affichageCase;
				
				affichageCase.setOnMouseClicked(e->{
					for(int m=0;m<nombre;m++) {
						for(int n=0;n<nombre;n++) {
							if(((Shape) niveau[m][n].getChildren().get(1)).getFill().equals(Color.web("#ebebeb"))) {
								((Shape) niveau[m][n].getChildren().get(0)).setFill(Color.web("#ebebeb"));
							}else {
								((Shape) niveau[m][n].getChildren().get(0)).setFill(Color.WHITE);
							}
						}
					}
					rectangleCaseGrille.setFill(bleu);
					affichageCase.getChildren().remove(rectangle);
					affichageCase.getChildren().remove(l);
					affichageCase.getChildren().add(rectangle);
					affichageCase.getChildren().add(l);
				});
				cases.add(affichageCase, b, a);
			}	
		}
		sousGrille.setMaxWidth(tailleSousGrille);
		sousGrille.setMaxHeight(tailleSousGrille);
		sousGrille.getChildren().addAll(rectangleFondSousGrille,cases);
		cases.setHgap(tailleBordSousGrille);
		cases.setVgap(tailleBordSousGrille);
		
		StackPane.setMargin(cases, new Insets(tailleBordSousGrille));
		return sousGrille;
	}
	public StackPane creerAffichageGrille(){
		StackPane grille = new StackPane();
		GridPane grilles = new GridPane();
		Rectangle rectangleFondGrille = new Rectangle(tailleGrille, tailleGrille,vert);
		
		grille.setMaxWidth(tailleGrille);
		grille.setMaxHeight(tailleGrille);
		
		grille.getChildren().addAll(rectangleFondGrille,grilles);
		for(int a=0;a<taille;a++) {
			for(int b=0;b<taille;b++) {
				StackPane sousGrille = creerAffichageSousGrille(a,b);
				grilles.add(sousGrille, a, b);
			}	
		}
		grilles.setHgap(tailleBordGrille);
		grilles.setVgap(tailleBordGrille);
		
		StackPane.setMargin(grilles, new Insets(tailleBordGrille));
		return grille;
	}

	public void labelValiderAction() {
		Temps.setEnPause(true);
		Temps.setTerminer(true);
		avertissement();
	}
	public void boutonPauseAction() {
		stoperTimer();
		Temps.setTerminer(true);
		Principale.toPause();
	}
	public void sceneAction(KeyEvent e) {
		obtenirTouches(e);
		if(quitter) { Principale.toPause();
		}else if(accepter&&labelValider.getTextFill().equals(bleu)) { labelValiderAction();
		}else if(accepter&&cerclePause.getFill().equals(bleu)) { boutonPauseAction();	
		}else {selectionnerCase();}
		changerValeurCase();
		
	}
	public static Temps getTimer() {
		return timer;
	}
	public static void setNumProfil(int numProfil) {
		Grille.numProfil = numProfil;
	}
	public static void setNumGrille(int numGrille) {
		Grille.numGrille = numGrille;
	}
}
