package profile;

import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.layout.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class Pause extends CommunM {
	private Label labelReprendre = new Label("Reprendre");
	private Label labelSelection = new Label("S�lection des niveaux");
	private Label labelRetour = new Label("Menu");
	private VBox root= new VBox();
	private StackPane boutonReprendre = new StackPane();
	private StackPane boutonSelection = new StackPane();
	private StackPane boutonRetour = new StackPane();
	private ArrayList<StackPane> menu = new ArrayList<StackPane>();

	public Pause() {
		menu.add(boutonReprendre);
		menu.add(boutonSelection);
		menu.add(boutonRetour);
		
		definirFenetre();
		boutonReprendre.setOnMouseClicked(e -> boutonReprendreAction());
		boutonSelection.setOnMouseClicked(e -> boutonSelectionAction());
		boutonRetour.setOnMouseClicked(e -> boutonRetourAction());
		this.getScene().setOnKeyPressed(e -> sceneAction(e));
	}
	
	public void selectionnerChoix() {
		int id;
		if(haut||bas){
			id = chercherChoix();
			if(id==-1) {
				survolerChoix(0);
			}else {
				if(haut){
					if(id==0) survolerChoix(menu.size()-1);
					else survolerChoix(id-1);
				}
				if(bas){
					if(id==menu.size()-1) survolerChoix(0);
					else survolerChoix(id+1);
				}
			}
		}else if(accepter){
			id = chercherChoix();
			if(id!=-1) menuAction(id);
		}
	}	
	public void survolerChoix(int i) {
		((Shape) menu.get(i).getChildren().get(0)).setFill(bleu);
	}
	public int chercherChoix() {
		int id=-1;
		int i=0;
		while((i<menu.size())&&id==-1) {
			if(((Shape) menu.get(i).getChildren().get(0)).getFill().equals(bleu)) {
				id=i;
				((Shape) menu.get(i).getChildren().get(0)).setFill((rouge));
			}else {
				i++;
			}
		}
		return id;
	}

	public Rectangle affichageRectangle() {
		Rectangle rect =  new Rectangle(550,100,rouge);
		rect.setArcHeight(30);
		rect.setArcWidth(30);
		return rect;
	}
	
	protected Parent creerContenu() {
		root.setStyle(couleurFond);
		
		boutonReprendre.getChildren().addAll(affichageRectangle(),labelReprendre);
		boutonSelection.getChildren().addAll(affichageRectangle(),labelSelection);
		boutonRetour.getChildren().addAll(affichageRectangle(),labelRetour);
		
		labelReprendre.setFont(cooperHewitt35);
		labelSelection.setFont(cooperHewitt35);
		labelRetour.setFont(cooperHewitt35);
		
		root.setPadding(new Insets(10));
		root.setSpacing(50);
		root.setAlignment(Pos.CENTER);
		root.getChildren().addAll(boutonReprendre,boutonSelection,boutonRetour);

		return root;
	}
	public void menuAction(int i) {
		switch(i) {
		case 0: boutonReprendreAction(); break;
		case 1: boutonSelectionAction(); break;
		case 2: boutonRetourAction(); break;
		}
	}
	public void boutonReprendreAction() {
		Grille.reprendreTimer();
		Principale.toGrille();
	}
	public void boutonSelectionAction() {
		Temps.setTerminer(true);
		Grille.stoperTimer();
		Principale.toNiveaux();
	}
	public void boutonRetourAction() {
		Temps.setTerminer(true);
		Grille.stoperTimer();
		Principale.toMenu();
	}
	public void sceneAction(KeyEvent e) {
		obtenirTouches(e);
		selectionnerChoix();
	}
}
